﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Logger.Models
{
    public class ProcessScanner
    {
        public IEnumerable<int> GetIdsOfRunningProcesses()
        {
            return 
                from instance in Process.GetProcesses()
                where instance != null
                select instance.Id;
        }

        public Process GetCurrentProcess()
        {
            var pid = 0;
            try
            {
                var desk = GetForegroundWindow();
                GetWindowThreadProcessId(desk, ref pid);
            }
            catch
            {
                // ignored
            }

            return Process.GetProcessById(pid);
        }

        public async Task<Entity.Process> GetCurrentProcessInfoAsync()
        {
            return await Task.Run(() =>
            {
                try
                {
                    var window = GetCurrentProcess();
                    return new Entity.Process(
                        window.Id,
                        window.ProcessName,
                        window.StartTime,
                        window.MainWindowTitle);
                }
                catch
                {
                    return new Entity.Process(0, "System process", DateTime.Now, "System process");
                }
            });
        }

        public async Task<DateTime> GetClosingTimeAsync(Entity.Process process)
        {
            return await Task.Factory.StartNew(
                () => {
                    if(process.Id == 0) return DateTime.Now;
                    while (true)
                    {
                        try
                        {
                            System.Threading.Thread.Sleep(2000);
                            var processes = GetIdsOfRunningProcesses();
                            var contains = processes.Contains(process.Id);
                            if (!contains)
                            {
                                return DateTime.Now;
                            }
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                        
                }
            );
        }

        #region #dll
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        public static extern UInt32 GetWindowThreadProcessId(IntPtr hwnd, ref Int32 pid);
        #endregion
    }
}
