﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Logger.Abstractions;
using Logger.Annotations;
using Logger.Models.Entity;

namespace Logger.Models.Managers
{
    public class ProcessesVisibilityManager : INotifyPropertyChanged
    {
        private readonly IDataWorker _dataWorker;

        private ObservableCollection<ProcessVisibility> _processes;
        public ObservableCollection<ProcessVisibility> Processes
        {
            get => _processes;
            set
            {
                _processes = value;
                OnPropertyChanged(nameof(Processes));
            }
        }

        public IEnumerable<ProcessVisibility> VisibleProcesses => Processes.Where(x => x.IsVisible);
        public IEnumerable<ProcessVisibility> HiddenProcesses => Processes.Where(x => !x.IsVisible);

        public ProcessesVisibilityManager(IDataWorker dataWorker)
        {
            _dataWorker = dataWorker;
            _processes = new ObservableCollection<ProcessVisibility>(_dataWorker.GetAll<ProcessVisibility>());
            _processes.CollectionChanged += Processes_Changed;
        }

        private void Processes_Changed(object sender, NotifyCollectionChangedEventArgs e)
            => _dataWorker.SaveAll(Processes);

        public void AddIfNotExist(Process process)
        {
            if (Processes.Any(x => x.Name == process.Name)) return;
            var processVisibility = ProcessVisibility.Cast(process);
            Processes.Insert(0, processVisibility);
        }

        public void ChangeVisibility(Process process)
        {
            var item = Processes.First(x => x.Name == process.Name);
            item.IsVisible = !item.IsVisible;
            _dataWorker.SaveAll(Processes);
        }
        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}