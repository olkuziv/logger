﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Logger.Abstractions;
using Logger.Annotations;
using Logger.Models.Entity;

namespace Logger.Models.Managers
{
    public class ProcessesManager : INotifyPropertyChanged
    {
        private readonly IDataWorker _dataWorker;

        private Func<Process, bool> _condition;
        public Func<Process, bool> Condition
        {
            get => _condition;
            set
            {
                _condition = value;                      
                OnPropertyChanged(nameof(Processes));
            }
        }

        public ObservableCollection<Process> Processes 
            => Condition == null ? AllProcesses : new ObservableCollection<Process>(AllProcesses.Where(Condition));

        private ObservableCollection<Process> AllProcesses { get; set; }

        public ProcessesManager(IDataWorker dataWorker)
        {
            _dataWorker = dataWorker;
            LoadData();
            AllProcesses.CollectionChanged += Processes_Changed;
        }

        private void Processes_Changed(object sender, NotifyCollectionChangedEventArgs e) 
            => _dataWorker.SaveAll(AllProcesses);

        public Process Add(Process process)
        {
            if (AllProcesses.Contains(process))
                return AllProcesses.First(x => x.Equals(process));

            AllProcesses.Insert(0, process);
            OnPropertyChanged(nameof(Processes));
            return process;
        }

        public void SetFinishTime(Process process, DateTime dateTime)
        {
            if (process.Finish == null)
                process.Finish = dateTime;
            OnPropertyChanged(nameof(Processes));
        }
        public void LoadData()
        {
            AllProcesses?.Clear();
            AllProcesses = new ObservableCollection<Process>(_dataWorker.GetAll<Process>());
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
