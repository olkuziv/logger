﻿using System;

namespace Logger.Models.Entity
{
    public class ProcessVisibility : Process
    {
        public ProcessVisibility(int id, string name, DateTime start, string mainWindowTitle) 
            : base(id, name, start, mainWindowTitle)
        {}
        public bool IsVisible
        {
            get => _isVisible;
            set
            {
                _isVisible = value;
                OnPropertyChanged(nameof(IsVisible));
            }
        }
        private bool _isVisible = true;

        public override bool Equals(object obj)
        {
            var visibility = obj as ProcessVisibility;
            var ist = visibility != null && base.Equals(obj);
            return ist;
        }


        public static ProcessVisibility Cast(Process p) => new ProcessVisibility(p.Id, p.Name, p.Start, p.MainWindowTitle);

        public override int GetHashCode()
        {
            return 624022166 + base.GetHashCode();
        }

    }
}
