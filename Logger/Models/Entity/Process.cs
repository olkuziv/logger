﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Logger.Annotations;
using Newtonsoft.Json;

namespace Logger.Models.Entity
{
    public class Process : INotifyPropertyChanged
    {
        #region local vars
        private int _id;
        private string _name;
        private DateTime _start;
        private DateTime? _finish;
        private string _mainWindowTitle;
        #endregion
        public Process(int id, string name, DateTime start, string mainWindowTitle)
        {
            Id = id;
            Name = name;
            Start = start;
            MainWindowTitle = mainWindowTitle;
        }

        public int Id
        {
            get => _id;
            set
            {
                if (value == _id) return;
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public DateTime Start
        {
            get => _start;
            set
            {
                if (value == _start) return;
                _start = value; 
                OnPropertyChanged(nameof(Start));
            }
        }
        public DateTime? Finish
        {
            get => _finish;
            set
            {
                if (value == _finish) return;
                _finish = value; 
                OnPropertyChanged(nameof(Finish));
                OnPropertyChanged(nameof(Delta));
            }
        }
        public string MainWindowTitle
        {
            get => _mainWindowTitle;
            set
            {
                if (value == _mainWindowTitle) return;
                _mainWindowTitle = value;
                OnPropertyChanged(nameof(MainWindowTitle));
            }
        }

        #region Ignoder Json props
        [JsonIgnore]
        public string Delta
        {
            get
            {
                var temp = (Finish - Start);
                return temp.HasValue ? temp.Value.ToString(@"hh\:mm\:ss") : " ";
            }
        }
        #endregion

        #region Overrides
        public override bool Equals(object obj)
        {
            return obj is Process process &&
                   Name == process.Name &&
                   Start.Ticks == process.Start.Ticks;
        }

        public override int GetHashCode()
        {
            var hashCode = 1477964895;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + Start.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return Name + Start + MainWindowTitle;
        }
        #endregion

        #region OnPropertyChanger Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
