﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Logger.Abstractions;
using Logger.Annotations;
using Newtonsoft.Json;

namespace Logger.Models.DataWorkers
{
    public class JsonNewtonsoftWorker : IDataWorker, INotifyPropertyChanged
    {
        private TimeSpan _getTime;
        private TimeSpan _saveTime;

        public TimeSpan GetTime
        {
            get => _getTime;
            set
            {
                _getTime = value;
                OnPropertyChanged(nameof(GetTime));
            }
        }
        public TimeSpan SaveTime
        {
            get => _saveTime;
            set
            {
                _saveTime = value; 
                OnPropertyChanged(nameof(SaveTime));
            }
        }

        public IEnumerable<T> GetAll<T>()
        {
            Stopwatch sw = Stopwatch.StartNew();

            var path = typeof(T).ToString().Split('.').Last() + ".json";
            if (!File.Exists(path))
                return new List<T>();

            var objects = File.ReadAllText(path);
            var objs = string.IsNullOrEmpty(objects) ? new List<T>() : JsonConvert.DeserializeObject<List<T>>(objects);

            sw.Stop();
            if (GetTime < sw.Elapsed) GetTime = sw.Elapsed;
            return objs;
        }

        public void SaveAll<T>(IEnumerable<T> list)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var path = typeof(T).ToString().Split('.').Last() + ".json";
            var objects = JsonConvert.SerializeObject(list,Formatting.Indented);
            File.WriteAllText(path, objects);

            sw.Stop();
            if (SaveTime < sw.Elapsed) SaveTime = sw.Elapsed;
        }

        public void ClearHistory<T>()
        {
            var path = typeof(T).ToString().Split('.').Last() + ".json";
            File.Delete(path);
        }

        public void ClearHistory()
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.json", SearchOption.TopDirectoryOnly);
            foreach (var filePath in files)
            {
                File.Delete(filePath);
            }
        }

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
