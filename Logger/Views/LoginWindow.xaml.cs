﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Logger.Annotations;

namespace Logger.Views
{
    public partial class LoginWindow : INotifyPropertyChanged
    {
        private string _pass;

        public dynamic Pass
        {
            get => _pass;
            set
            {
                _pass = value;
                OnPropertyChanged(nameof(Pass));
            }
        }

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void UIElement_OnKeyUp(object sender, KeyEventArgs e)
        {
            Pass = PasswordBox.Password;
            if (Pass != ".") return;

            new MainWindow().Show();
            Close();
        }
        #region OnPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
