﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using Logger.Abstractions;
using Logger.Annotations;
using Logger.Models;
using Logger.Models.DataWorkers;
using Logger.Models.Entity;
using Logger.Models.Managers;
using MessageBox = System.Windows.MessageBox;

// ReSharper disable ArrangeThisQualifier

namespace Logger.Views
{
    public partial class MainWindow : INotifyPropertyChanged
    {
        private Visibility _isAdmin = Visibility.Hidden;
        public Visibility IsAdmin
        {
            get => _isAdmin;
            set
            {
                _isAdmin = value;
                OnPropertyChanged(nameof(IsAdmin));
            }
        }

        public ProcessScanner Scanner { get; } 
        public IDataWorker DataWorker { get; } 
        public ProcessesVisibilityManager VisibilityManager { get; }
        public ProcessesManager ProcessesManager { get; }

        public MainWindow()
        {
            {
                Scanner = new ProcessScanner();
                DataWorker = new JsonNewtonsoftWorker();
                VisibilityManager = new ProcessesVisibilityManager(DataWorker);
                ProcessesManager = new ProcessesManager(DataWorker);
            }
            {
                var ni = new NotifyIcon();
                ni.Icon = Properties.Resources.Papirus_Team_Papirus_Mimetypes_Qgis_qpt;
                ni.Visible = true;
                ni.Click += Icon_Click;
            }
            {
                var timer = new DispatcherTimer();
                timer.Tick += DispatcherTimer_Tick;
                timer.Interval = new TimeSpan(0, 0, 2);
                timer.Start();
            }
            InitializeComponent();
            DataContext = this;
        }
        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
                this.Hide();

            base.OnStateChanged(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void Icon_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = WindowState.Normal;
        }

        private async void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            var proc = await Scanner.GetCurrentProcessInfoAsync();
            if (proc == null) return;

            var cur = ProcessesManager.Add(proc);
            VisibilityManager.AddIfNotExist(proc);

            if (cur.Finish != null) return;
            var time = await Scanner.GetClosingTimeAsync(cur);
            ProcessesManager.SetFinishTime(cur, time);
        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(sender is Calendar calendar) || !calendar.SelectedDate.HasValue) return;
            var date = calendar.SelectedDate.Value;
            ProcessesManager.Condition = x => x.Start.Date == date.Date;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            IsAdmin = Visibility.Visible;
        }

        private void ButtonHide_OnClick(object sender, RoutedEventArgs e)
        {
            IsAdmin = Visibility.Hidden;
        }

        private void ProcessVisibilities_Click(object sender, MouseButtonEventArgs e)
        {
            var lbx = sender as System.Windows.Controls.ListBox;
            var it = (Process)lbx?.SelectedItem;
            VisibilityManager.ChangeVisibility(it);
        }

        private void UpdateVisibilities_Click(object sender, RoutedEventArgs e)
        {
            var hiddenItems = VisibilityManager.HiddenProcesses;
            ProcessesManager.Condition = x => !(hiddenItems.Any(y => y.Name == x.Name));
        }
        #region OnPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private void ClearHistory_Click(object sender, RoutedEventArgs e)
        {
            DataWorker.ClearHistory<Process>();
            ProcessesManager.LoadData();
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            var input = new LoginWindow();
            var exp = (Expander) sender;
            input.ShowDialog();
            try
            {
                if (string.IsNullOrEmpty(input.Pass))
                {
                    exp.IsExpanded = false;
                }
                else if (input.Pass.ToString() != "2112")
                {
                    exp.IsExpanded = false;
                }
            }
            catch 
            {
                exp.IsExpanded = false;

            }

        }
    }
}
