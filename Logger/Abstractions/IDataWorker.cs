﻿using System;
using System.Collections.Generic;

namespace Logger.Abstractions
{
    public interface IDataWorker
    {
        TimeSpan GetTime { get; set; }
        TimeSpan SaveTime { get; set; }

        IEnumerable<T> GetAll<T>();
        void SaveAll<T>(IEnumerable<T> list);

        void ClearHistory<T>();
        void ClearHistory();
    }
}
