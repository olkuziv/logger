﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StatusSync.Forms
{
    public partial class MainForm : Form
    {
        Helpers.ProcessScaner ps;
        Abstractions.IDataWorker dataWorker;

        public DateTime StartTime;
        private bool _onlyVisible = true;
        public MainForm()
        {
            InitializeComponent();
            ps = new Helpers.ProcessScaner();
            dataWorker = new Helpers.XmlDataWorker();
            StartTime = DateTime.Now;
            LoadHistory();
            string end = Properties.Settings.Default.ProgramEnd;
            if (end != "")
            {
                DateTime start, finish;
                start = DateTime.Parse(end.Split('|')[0]);
                finish = DateTime.Parse(end.Split('|')[1]);
                dataWorker.AddInHistory("ElsePrograms", start, finish);
                Properties.Settings.Default.ProgramEnd = "";
                Properties.Settings.Default.Save();
            }
            timer1.Start();
        }

        #region #MyMethods
        private void LoadHistory(IEnumerable<Model.History> history = null)
        {
            IEnumerable<Model.Process> processList = dataWorker.GetProcesses();
            if (history == null)
            {
                history = dataWorker.GetHistory(_onlyVisible);
            }
            try
            {
                dgv1.Rows.Clear();

                var temp = history.Select(x => new {
                    name = processList.Where(y => y.Id == x.ProcessId).First().Name,
                    started = x.Start,
                    finished = x.Finish,
                    time = x.DTime
                }).ToList();
                foreach (var item in temp)
                {
                    dgv1.Rows.Add(item.name, item.started, item.finished, item.time);
                }
            }
            catch { }
        }
        private IEnumerable<string> getSelectedProceses()
        {
            for (int i = 0; i < dgv1.SelectedRows.Count; i++)
            {
                yield return dgv1.SelectedRows[i].Cells[0].Value.ToString();
            }
        }
       
        public async void GetClosingTimeAsync(System.Diagnostics.Process process)
        {
            try
            {
                var name = process.ProcessName;
                var start = process.StartTime;

                var t = await ps.GetClosingTimeAsync(process);
                dataWorker.AddInHistory(name, start, t);
                System.Threading.Thread.Sleep(500);
                LoadHistory();
            }
            catch { }
        }

        #endregion

        System.Diagnostics.Process tempProc;
        private void timer1_Tick(object sender, EventArgs e)
        {
            var process = ps.GetCurrentProcess();

            if (tempProc == null || tempProc.ProcessName != process.ProcessName)
            {
                tempProc = process;
                GetClosingTimeAsync(process);
            }
        }

        private void status_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            status.Visible = false;
            this.Show();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            var data = dataWorker.GetHistory(_onlyVisible).Where(x => (x.Finish.Date <= monthCalendar1.SelectionEnd.Date && x.Finish.Date >= monthCalendar1.SelectionStart.Date));
            LoadHistory(data);
        }

        private void btnShowHistory_Click(object sender, EventArgs e)
        {
            LoadHistory(dataWorker.GetHistory(_onlyVisible));
        }

        private void btnShowSelected_Click(object sender, EventArgs e)
        {
            if (cbxEnableCalendar.Checked)
            {
                var selectedNames = getSelectedProceses().ToList();
                var selectedIds = dataWorker.GetProcesses().Where(x => selectedNames.Contains(x.Name)).Select(x => x.Id);

                LoadHistory(dataWorker.GetHistory(_onlyVisible)
                    .Where(x => selectedIds.Contains(x.ProcessId))
                    .Where(y => (y.Finish.Date <= monthCalendar1.SelectionEnd.Date && y.Finish.Date >= monthCalendar1.SelectionStart.Date)));
            }
            else
            {
                var selectedNames = getSelectedProceses().ToArray();
                var selectedIds = dataWorker.GetProcesses().Where(x => selectedNames.Contains(x.Name)).Select(x=>x.Id).ToArray();
                
                LoadHistory(dataWorker.GetHistory(_onlyVisible).Where(x => selectedIds.Contains(x.ProcessId)));
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Properties.Settings.Default.ProgramEnd = $"{StartTime}|{DateTime.Now}";
            Properties.Settings.Default.Save();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Login lgn = new Login();
            lgn.Show();
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            this.Hide();
            status.Visible = true;
        }

        private void Main_Activated(object sender, EventArgs e)
        {
            LoadHistory();
        }

        private void cbxEnableCalendar_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxEnableCalendar.Checked)
            {
                monthCalendar1.Enabled = true;
                var data = dataWorker.GetHistory(_onlyVisible).Where(x => (x.Finish.Date <= monthCalendar1.SelectionEnd.Date && x.Finish.Date >= monthCalendar1.SelectionStart.Date));
                LoadHistory(data);
            }
            else
            {
                monthCalendar1.Enabled = false;
                LoadHistory();
            }
        }
    }
}
