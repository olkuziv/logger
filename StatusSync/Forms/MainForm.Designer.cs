﻿namespace StatusSync.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSettings = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.NotifyIcon(this.components);
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.dgv1 = new System.Windows.Forms.DataGridView();
            this.dgvColumsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColumStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColumnFinish = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvColumTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnShowHistory = new System.Windows.Forms.Button();
            this.btnShowSelected = new System.Windows.Forms.Button();
            this.cbxEnableCalendar = new System.Windows.Forms.CheckBox();
            this.btnHide = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panel1.Controls.Add(this.btnHide);
            this.panel1.Controls.Add(this.btnSettings);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(702, 34);
            this.panel1.TabIndex = 5;
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.SystemColors.WindowText;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Location = new System.Drawing.Point(9, 4);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(25, 25);
            this.btnSettings.TabIndex = 1;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // status
            // 
            this.status.Icon = ((System.Drawing.Icon)(resources.GetObject("status.Icon")));
            this.status.Text = "Status Info";
            this.status.Visible = true;
            this.status.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.status_MouseDoubleClick);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Enabled = false;
            this.monthCalendar1.Location = new System.Drawing.Point(2, 79);
            this.monthCalendar1.Margin = new System.Windows.Forms.Padding(0);
            this.monthCalendar1.MaxSelectionCount = 31;
            this.monthCalendar1.MinDate = new System.DateTime(2018, 1, 1, 0, 0, 0, 0);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 6;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // dgv1
            // 
            this.dgv1.AllowUserToAddRows = false;
            this.dgv1.AllowUserToDeleteRows = false;
            this.dgv1.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dgv1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvColumsName,
            this.dgvColumStart,
            this.dgvColumnFinish,
            this.dgvColumTime});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgv1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dgv1.GridColor = System.Drawing.SystemColors.Control;
            this.dgv1.Location = new System.Drawing.Point(163, 34);
            this.dgv1.Name = "dgv1";
            this.dgv1.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Chartreuse;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv1.Size = new System.Drawing.Size(539, 456);
            this.dgv1.TabIndex = 7;
            // 
            // dgvColumsName
            // 
            this.dgvColumsName.HeaderText = "Назва процесу";
            this.dgvColumsName.Name = "dgvColumsName";
            this.dgvColumsName.ReadOnly = true;
            // 
            // dgvColumStart
            // 
            this.dgvColumStart.FillWeight = 160F;
            this.dgvColumStart.HeaderText = "Час старту";
            this.dgvColumStart.Name = "dgvColumStart";
            this.dgvColumStart.ReadOnly = true;
            this.dgvColumStart.Width = 130;
            // 
            // dgvColumnFinish
            // 
            this.dgvColumnFinish.HeaderText = "Час завершення";
            this.dgvColumnFinish.Name = "dgvColumnFinish";
            this.dgvColumnFinish.ReadOnly = true;
            this.dgvColumnFinish.Width = 130;
            // 
            // dgvColumTime
            // 
            this.dgvColumTime.HeaderText = "Загальний час";
            this.dgvColumTime.Name = "dgvColumTime";
            this.dgvColumTime.ReadOnly = true;
            this.dgvColumTime.Width = 130;
            // 
            // btnShowHistory
            // 
            this.btnShowHistory.BackColor = System.Drawing.SystemColors.Window;
            this.btnShowHistory.FlatAppearance.BorderSize = 0;
            this.btnShowHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowHistory.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShowHistory.Location = new System.Drawing.Point(7, 296);
            this.btnShowHistory.Name = "btnShowHistory";
            this.btnShowHistory.Size = new System.Drawing.Size(149, 40);
            this.btnShowHistory.TabIndex = 11;
            this.btnShowHistory.Text = "Показати весь список";
            this.btnShowHistory.UseVisualStyleBackColor = false;
            this.btnShowHistory.Click += new System.EventHandler(this.btnShowHistory_Click);
            // 
            // btnShowSelected
            // 
            this.btnShowSelected.BackColor = System.Drawing.SystemColors.Window;
            this.btnShowSelected.FlatAppearance.BorderSize = 0;
            this.btnShowSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowSelected.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShowSelected.Location = new System.Drawing.Point(7, 250);
            this.btnShowSelected.Name = "btnShowSelected";
            this.btnShowSelected.Size = new System.Drawing.Size(149, 40);
            this.btnShowSelected.TabIndex = 12;
            this.btnShowSelected.Text = "Показати лише обрані процеси";
            this.btnShowSelected.UseVisualStyleBackColor = false;
            this.btnShowSelected.Click += new System.EventHandler(this.btnShowSelected_Click);
            // 
            // cbxEnableCalendar
            // 
            this.cbxEnableCalendar.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbxEnableCalendar.Location = new System.Drawing.Point(7, 35);
            this.cbxEnableCalendar.Name = "cbxEnableCalendar";
            this.cbxEnableCalendar.Size = new System.Drawing.Size(153, 44);
            this.cbxEnableCalendar.TabIndex = 13;
            this.cbxEnableCalendar.Text = "Ввімкнути пошук за датою";
            this.cbxEnableCalendar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbxEnableCalendar.UseVisualStyleBackColor = true;
            this.cbxEnableCalendar.CheckedChanged += new System.EventHandler(this.cbxEnableCalendar_CheckedChanged);
            // 
            // btnHide
            // 
            this.btnHide.BackgroundImage = global::StatusSync.Properties.Resources.if_fullscreen_exit_326649;
            this.btnHide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnHide.FlatAppearance.BorderSize = 0;
            this.btnHide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHide.Location = new System.Drawing.Point(669, 4);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(25, 25);
            this.btnHide.TabIndex = 2;
            this.btnHide.Text = "  ";
            this.btnHide.UseVisualStyleBackColor = true;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(702, 490);
            this.Controls.Add(this.cbxEnableCalendar);
            this.Controls.Add(this.btnShowSelected);
            this.Controls.Add(this.btnShowHistory);
            this.Controls.Add(this.dgv1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Activated += new System.EventHandler(this.Main_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NotifyIcon status;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.DataGridView dgv1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumnFinish;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvColumTime;
        private System.Windows.Forms.Button btnShowHistory;
        private System.Windows.Forms.Button btnShowSelected;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.CheckBox cbxEnableCalendar;
    }
}