﻿using Microsoft.Win32;
using StatusSync.Properties;
using System;
using System.Windows.Forms;

namespace StatusSync.Forms
{
    public partial class Admin : Form
    {
        Abstractions.IDataWorker dataWorker;
        public Admin()
        {
            InitializeComponent();
            dataWorker = new Helpers.XmlDataWorker();
        }

        private void Admin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (tbxPass.Text != null && tbxPass.Text != "")
                Settings.Default.Pass = tbxPass.Text;

            Settings.Default.UpdateTime = Convert.ToInt16(numUpDown.Value);

            Settings.Default.Save();
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            tbxPass.Text = Settings.Default.Pass;

            cbxAutoRun.Checked = Settings.Default.AutoRun;

            foreach (var item in dataWorker.GetProcesses())
            {
                lbxProc.Items.Add(item.Name, item.Visible);
            }
        }

        private void cbxAutoRun_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxAutoRun.Checked)
            {
                RegistryKey myKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                myKey.SetValue("MyProgram", Application.ExecutablePath);
            }
            else
            {
                RegistryKey myKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                myKey.DeleteValue("MyProgram");
            }
            Settings.Default.AutoRun = cbxAutoRun.Checked;
            Settings.Default.Save();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
        }


        private void lbxProc_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (lbxProc.SelectedItems.Count == 1)
            {
                dataWorker.ChangeVisible(lbxProc.SelectedItem.ToString(), (e.NewValue == CheckState.Checked) ? true : false);
                //MessageBox.Show(lbxProc.SelectedItem.ToString() + ((e.NewValue == CheckState.Checked) ? true : false));
            }

        }
        
    }
}
