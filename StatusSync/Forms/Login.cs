﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StatusSync.Forms
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void tbxPass_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) Open();
        }

        void Open()
        {
            if (tbxPass.Text == Properties.Settings.Default.Pass)
            {
                new Admin().Show();
                this.Close();
            }
            else MessageBox.Show("Не правельний пароль!");
        }
    }
}
