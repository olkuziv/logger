﻿using System;
using System.Windows.Forms;

namespace StatusSync.Forms
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblUpdateTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAutoRun = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.tbxPass = new System.Windows.Forms.TextBox();
            this.cbxAutoRun = new System.Windows.Forms.CheckBox();
            this.lbxProc = new System.Windows.Forms.CheckedListBox();
            this.lblProcessList = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.numUpDown = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.78205F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.21795F));
            this.tableLayoutPanel1.Controls.Add(this.lblUpdateTime, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblAutoRun, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblPass, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbxPass, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxAutoRun, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbxProc, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblProcessList, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnClear, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.numUpDown, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96536F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96536F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.1732F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96536F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96536F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.96536F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(312, 333);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblUpdateTime
            // 
            this.lblUpdateTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUpdateTime.AutoSize = true;
            this.lblUpdateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblUpdateTime.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblUpdateTime.Location = new System.Drawing.Point(15, 245);
            this.lblUpdateTime.Name = "lblUpdateTime";
            this.lblUpdateTime.Size = new System.Drawing.Size(90, 32);
            this.lblUpdateTime.TabIndex = 10;
            this.lblUpdateTime.Text = "Частота обновлення ";
            this.lblUpdateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(24, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 32);
            this.label1.TabIndex = 8;
            this.label1.Text = "Очистити Історію";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAutoRun
            // 
            this.lblAutoRun.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAutoRun.AutoSize = true;
            this.lblAutoRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAutoRun.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblAutoRun.Location = new System.Drawing.Point(17, 53);
            this.lblAutoRun.Name = "lblAutoRun";
            this.lblAutoRun.Size = new System.Drawing.Size(85, 32);
            this.lblAutoRun.TabIndex = 6;
            this.lblAutoRun.Text = "Добавити в автозапуск";
            this.lblAutoRun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPass
            // 
            this.lblPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPass.AutoSize = true;
            this.lblPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPass.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPass.Location = new System.Drawing.Point(10, 15);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(100, 16);
            this.lblPass.TabIndex = 1;
            this.lblPass.Text = "Новий пароль";
            this.lblPass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbxPass
            // 
            this.tbxPass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbxPass.Location = new System.Drawing.Point(127, 12);
            this.tbxPass.Name = "tbxPass";
            this.tbxPass.Size = new System.Drawing.Size(178, 22);
            this.tbxPass.TabIndex = 2;
            // 
            // cbxAutoRun
            // 
            this.cbxAutoRun.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxAutoRun.AutoSize = true;
            this.cbxAutoRun.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbxAutoRun.Location = new System.Drawing.Point(123, 60);
            this.cbxAutoRun.Name = "cbxAutoRun";
            this.cbxAutoRun.Size = new System.Drawing.Size(26, 17);
            this.cbxAutoRun.TabIndex = 7;
            this.cbxAutoRun.Text = "     ";
            this.cbxAutoRun.UseVisualStyleBackColor = true;
            this.cbxAutoRun.CheckedChanged += new System.EventHandler(this.cbxAutoRun_CheckedChanged);
            // 
            // lbxProc
            // 
            this.lbxProc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbxProc.FormattingEnabled = true;
            this.lbxProc.Location = new System.Drawing.Point(127, 95);
            this.lbxProc.Name = "lbxProc";
            this.lbxProc.Size = new System.Drawing.Size(177, 94);
            this.lbxProc.TabIndex = 5;
            this.lbxProc.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lbxProc_ItemCheck);
            // 
            // lblProcessList
            // 
            this.lblProcessList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblProcessList.AutoSize = true;
            this.lblProcessList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProcessList.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblProcessList.Location = new System.Drawing.Point(3, 126);
            this.lblProcessList.Name = "lblProcessList";
            this.lblProcessList.Size = new System.Drawing.Size(113, 32);
            this.lblProcessList.TabIndex = 3;
            this.lblProcessList.Text = "Список видимих процесів";
            this.lblProcessList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.Location = new System.Drawing.Point(178, 203);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Очистити";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // numUpDown
            // 
            this.numUpDown.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numUpDown.Location = new System.Drawing.Point(156, 251);
            this.numUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUpDown.Name = "numUpDown";
            this.numUpDown.Size = new System.Drawing.Size(120, 20);
            this.numUpDown.TabIndex = 12;
            this.numUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 333);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Admin_FormClosing);
            this.Load += new System.EventHandler(this.Admin_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDown)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.TextBox tbxPass;
        private System.Windows.Forms.Label lblProcessList;
        private System.Windows.Forms.CheckedListBox lbxProc;
        private System.Windows.Forms.Label lblAutoRun;
        private System.Windows.Forms.CheckBox cbxAutoRun;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblUpdateTime;
        private System.Windows.Forms.NumericUpDown numUpDown;
    }
}