﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatusSync.Model
{
    [Serializable]
    public class Process
    {
        /// <summary>
        /// Пустий конструктор для зчитування з XML
        /// </summary>
        public Process() { }
        public Process(bool serialize = false) {
            if (serialize)
            {
                if (DataRepository.Processes.Any())
                    Id = DataRepository.Processes.Last().Id + 1;
                else Id = 0;
                DataRepository.Processes.Add(this);
            }
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public bool Visible { get; set; }

        //public List<int> DeleteHistoryId() {
        //    HistoryIds.Remove(id)
        //    return HistoryIds;
        //}
    }
}
