﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StatusSync
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //new Helpers.XmlDataWorker().SaveAll(Model.DataRepository.History);
            //new Helpers.XmlDataWorker().SaveAll(Model.DataRepository.Processes);
            Application.Run(new Forms.MainForm());
        }
    }
}
