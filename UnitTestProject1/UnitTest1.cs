﻿using System;
using System.Linq;
using Logger.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ProcessScaner_GetIdsOfRuningProcesses_ExistAny()
        {
            var test = new ProcessScanner();

            var ids = test.GetIdsOfRunningProcesses();

            Assert.IsTrue(ids.Any());
            Assert.IsTrue(ids.ElementAt(0)!=0);
        }

        [TestMethod]
        public void ProcessScaner_GetCurrentProcces_IsNotNull()
        {
            var test = new ProcessScanner();

            var process = test.GetCurrentProcess();

            Assert.IsNotNull(process);
        }

        [TestMethod]
        public void ProcessScaner_GetProcessInfo_IsNotNullName()
        {
            var test = new ProcessScanner();

            var data = test.GetCurrentProcessInfoAsync();

            var name = data.ProcessName;

            Assert.IsFalse(string.IsNullOrEmpty(name));
        }


    }
}
